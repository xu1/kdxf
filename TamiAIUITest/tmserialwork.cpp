#include "tmserialwork.h"
#include <QDebug>
#include "mainwindow.h"
#include "mgzip.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

TmSerialWork::TmSerialWork(QObject *parent) : QObject(parent)
{
    m_bInit = false;
    bIsWakeUp = false;
    pr_httpGet = new QNetworkAccessManager(this);
    connect(pr_httpGet,SIGNAL(finished(QNetworkReply*)),this,SLOT(tuling_replyFinish(QNetworkReply*)));

    g_pMediaCtrl = new CTMMediaCtrl();
    g_pMediaCtrl->init();
    m_bTTSIsUseKdxf = false;
    m_bIsReplyByTrio = true;
}

void TmSerialWork::slot_initSerial(QString strCom)
{
    // init serial port
    m_pSer = new QSerialPort();
    m_pSer->setPortName(strCom);
    m_pSer->setBaudRate(QSerialPort::Baud115200);
    m_pSer->setDataBits(QSerialPort::Data8);
    m_pSer->setStopBits(QSerialPort::OneStop);
    m_pSer->setParity(QSerialPort::NoParity);

    m_bInit = m_pSer->open(QIODevice::ReadWrite);

    if( !m_bInit){
        qDebug()<<strCom+"串口打开失败，请检查接口是否接好或者被其他程序占用!";
        return;
    }
    // recv data
    connect(m_pSer, SIGNAL(readyRead()), this, SLOT(slot_readSerialData()));

}

void TmSerialWork::slot_recordVoice(int nTimeS)
{
    QString strQuery = QString("{\"type\": \"save_audio\",\"content\": {\"save_len\": %1}}").arg(nTimeS);
    QByteArray sendData;
    if(createMsg(0x05,strQuery,sendData))
        slot_writeSerialData(sendData);
}

void TmSerialWork::output(const QByteArray &b)
{
    QString str;
    foreach (uchar c, b) {
        str += QString::number((long)c,16) + " ";
    }
    qDebug()<<str;
}

bool TmSerialWork::verify(const QByteArray &data)
{
    QByteArray b(data);
    uchar c = b[b.size()-1];
    return c == InternalCheckCode(b.left(b.size()-1));
}

bool TmSerialWork::getJsonString(const QJsonObject &sourceObj,const QString &key, QString &destStr)
{
    if(!sourceObj.contains(key))
        return false;
    QJsonValue destValue = sourceObj.value(key);
    if(!destValue.isString())
        return false;
    destStr = destValue.toString();
    return true;
}

bool TmSerialWork::getJsonObj(const QJsonObject &sourceObj,const QString &key, QJsonObject &destObj)
{
    if(!sourceObj.contains(key))
        return false;
    QJsonValue destValue = sourceObj.value(key);
    if(!destValue.isObject())
        return false;
    destObj = destValue.toObject();
    return true;
}

int TmSerialWork::getAsrResult(const QString &data,QString &result)
{
    QJsonParseError json_error;
    QJsonDocument parse_doucment = QJsonDocument::fromJson(data.toUtf8(), &json_error);
    if(json_error.error != QJsonParseError::NoError)
    {
        qDebug()<<"error!!!!"<<data;
        return 1;
    }

    QJsonObject jsObj = parse_doucment.object();
    if( !jsObj.contains("type") ){
        return 2;
    }
    // process cmd
    QString strCmd = jsObj.value("type").toString();
    if( strCmd != "aiui_event" )
        return 3;
    //判断sub：
    QJsonObject contentObj;
    if(!getJsonObj(jsObj,"content",contentObj))
        return 4;
    //唤醒
    if(contentObj.contains("eventType"))
    {
        QJsonValue eventTypeV = contentObj.value("eventType");
        if(eventTypeV.isDouble())
        {
            if(4==eventTypeV.toInt())
            {
                getWakeUpTTS();
                return 20;
            }
        }
    }

    QJsonObject infoObj;
    if(!getJsonObj(contentObj,"info",infoObj))
        return 5;
    if(!infoObj.contains("data"))
        return 6;
    QJsonValue dataV = infoObj.value("data");
    if(!dataV.isArray())
        return 7;
    QJsonArray dataArr = dataV.toArray();

    if(dataArr.size()<1)
    {
        return 8;
    }
    QJsonValue dataItemV = dataArr[0];
    if(!dataItemV.isObject())
        return 9;
    QJsonObject paramsObj;
    if(!getJsonObj(dataItemV.toObject(),"params",paramsObj))
        return 10;
    QString strSub;
    if(!getJsonString(paramsObj,"sub",strSub))
        return 11;
    if("nlp" != strSub)
        return 12;

    //获取text：
    QJsonObject resultObj;
    if(!getJsonObj(contentObj,"result",resultObj))
        return 13;
    QJsonObject intentObj;
    if(!getJsonObj(resultObj,"intent",intentObj))
        return 14;
    QString strText;
    if(!getJsonString(intentObj,"text",strText))
        return 15;
    qDebug()<<"********ASR result:"<<strText.toStdString().data();
    result = strText;
    return 0;
}

bool TmSerialWork::createMsg(uchar msgtype, const QByteArray &msg, QByteArray &dataOut)
{
    static uchar msgId = 0;
    uchar b[] = {0xA5, 0x01, msgtype, 0x00, 0x00, msgId++,0x00};
    dataOut.append(QByteArray((const char*)b,sizeof(b)));
    dataOut.append(msg);
    if(!countBuffLength(dataOut))
    {
//        QMessageBox::about(0,"err","长度设置错误");
        qDebug()<<"err countBuffLength";
        return false;
    }
    dataOut.append(InternalCheckCode(dataOut));
    return true;
}

void TmSerialWork::handshake()
{
    uchar buff[] = {0xa5, 0x01, 0x01, 0x04, 0x00, 0xDD, 0x00, 0xa5, 0x00, 0x00, 0x00};
    QByteArray data((const char*)buff,sizeof(buff));
    data.append(InternalCheckCode(data));
    slot_writeSerialData(data);
}

bool TmSerialWork::createMsg(uchar msgtype, const QString &msg, QByteArray &dataOut)
{
//    return createMsg(msgtype,msg.toUtf8(),dataOut);
    static uchar msgId = 0;
    uchar b[] = {0xA5, 0x01, msgtype, 0x00, 0x00, msgId++,0x00};
    dataOut.append(QByteArray((const char*)b,sizeof(b)));
    dataOut.append(msg.toUtf8());
    if(!countBuffLength(dataOut))
    {
//        QMessageBox::about(0,"err","长度设置错误");
        qDebug()<<"err countBuffLength";
        return false;
    }
    dataOut.append(InternalCheckCode(dataOut));
    return true;
}

bool TmSerialWork::countBuffLength(QByteArray &buff)
{
    int ns = buff.size();
    if(ns>8)
    {
        uint n = buff.size()-7;
        buff[3] = 0xFF & n;
        buff[4] = 0xFF & (n>>8);
        return true;
    }
    return false;
}

uchar TmSerialWork::InternalCheckCode(QByteArray buff)
{
    // 计算内部校检码
    uchar check_code = 0;
    foreach (char c, buff) {
        check_code += c;
    }

    check_code = ~check_code + 1;
    return check_code;
}

QByteArray TmSerialWork::confirm(uchar IdLow,uchar IdHigh)
{
    uchar buff[] = {0xA5, 0x01, 0xff, 0x04, 0x00,IdLow,IdHigh,0xA5,0x00,0x00,0x00};
    QByteArray data((const char*)buff,sizeof(buff));
    data.append(InternalCheckCode(data));
    return data;
}

void TmSerialWork::getWakeUpTTS()
{
    QList<QString> strList;
    strList.append(QString::fromLocal8Bit("我在呢"));
    strList.append(QString::fromLocal8Bit("怎么了"));
    strList.append(QString::fromLocal8Bit("你好啊"));
    strList.append(QString::fromLocal8Bit("有什么可以帮助你的呢"));
    int index = g_nGetRandom(0,3);


    m_strWakeUp = strList.at(index);
    qDebug()<<"m_strWakeUp"<<m_strWakeUp.toStdString().data();

    handshake();
    bIsWakeUp = true;
    if(m_bTTSIsUseKdxf){
        g_pMediaCtrl->media_play_tts(m_strWakeUp);
        m_strWakeUp = "";
        return;
    }


}

void TmSerialWork::setTTSUseBy(bool m_bIsUseKdxf)
{
    if(m_bIsUseKdxf){
        m_bTTSIsUseKdxf = true;
    }else{
        m_bTTSIsUseKdxf = false;
    }
}

void TmSerialWork::prase(const QByteArray &data)
{
    //uint msgSize = ((data[4]<<8)|data[3]) & 0XFF;//消息长度
    uint msgId = ((0xFF00 & (data[6]<<8)) |(data[5]&0xFF)) & 0XFF;//消息ID
    QByteArray msg = data.mid(7,data.size()-7-1);//消息内容
    switch ((uchar)data[2]) {
    case 0x01://请求握手
    {
        slot_writeSerialData(confirm(uchar(data[5]),uchar(data[6])));
        //qDebug()<<"确认握手";
        //output(d);
        break;
    }
    case 0x02://wifi配置
        qDebug()<<"wifi:"<<QString::fromUtf8(msg);
        break;
    case 0x03://AIUI配置

        break;
    case 0x04://AIUI消息，使用gzip编码解码
    {
        QString strResult;
        int n = getAsrResult(MGzip::muncompress(msg,10240),strResult);
        if(0==n){
            if(strResult.size() > 1){
                emit sig_show(1,strResult);
                slot_writeSerialData(confirm(uchar(data[5]),uchar(data[6])));
                QString strUrl;
                if(m_bIsReplyByTrio){
                    strUrl = QString("http://sandbox.sanjiaoshou.net/Api/chat?who=tami&userID=%1&sentence=%2")
                        .arg("tami_111").arg(strResult);
                }else{
                    strUrl = QString("http://www.tuling123.com/openapi/api?key=652f89bd9d9248f99e46d96d5b649228&userid=%1&info=%2")
                        .arg("abc123").arg(strResult);
                }
                pr_httpGet->get(QNetworkRequest(QUrl(strUrl)));
            }
        }else{
            //qDebug()<<"MGzip::muncompress(msg,10240)::"<<MGzip::muncompress(msg,10240);
        }
        //qDebug()<<"=====:"<<MGzip::muncompress(msg,10240);
        break;
    }
    case 0x05://主控消息
        //qDebug()<<"主控:"<<msgSize<<msgId<<"=="<<MGzip::muncompress(msg,1024);
        break;
    case 0xFF://确认消息
        //output(data);
        if(msgId == uint(0xDD) && !m_bTTSIsUseKdxf){
            if(bIsWakeUp){
                play_tts(m_strWakeUp);
                bIsWakeUp = false;
                m_strWakeUp = "";
                break;
            }
            play_tts(m_strAnswer);
            emit sig_show(2,m_strAnswer);
            m_strAnswer = "";
        }
        break;
    default:
        break;
    }
}

Type_Analysis TmSerialWork::slot_readSerialData()
{
    m_serBuff += m_pSer->readAll();

    //长度至少为8字节：
    if(m_serBuff.size()<8)
        return Type_Analysis_Err;
    int nHead = m_serBuff.indexOf((char)0xA5);
    if(nHead>-1 && m_serBuff[nHead+1]==(char)0X01)
    {
        if(nHead>0)
        {
            m_serBuff.remove(0,nHead);
        }
    }
    //解析：
    uint msgSize = (((m_serBuff[4]<<8)&0XFF00)|(m_serBuff[3] & 0xFF)) & 0XFFFF;//消息长度
    //qDebug()<<"*----------"<<msgSize;
    //output(m_serBuff);
    if(msgSize>uint(m_serBuff.size()-7-1))
    {
        return Type_Analysis_Err;
    }
    QByteArray data = m_serBuff.mid(0,7+1+msgSize);
    m_serBuff.remove(0,7+1+msgSize);
    //检测校验：
    if(!verify(data))
    {
        qDebug()<<"check failed:"<<msgSize<<" "<<data.size();
        output(data);
        return Type_Analysis_Err;
    }

    prase(data);

    return Type_Analysis_Sccess;
}

void TmSerialWork::tuling_replyFinish(QNetworkReply *reply)
{
    QByteArray bReply = reply->readAll();
    m_strAnswer = parser_tuling_json(bReply);
//    if( m_strAnswer.length() > 100 ){
//        m_strAnswer = m_strAnswer.left(100);
//        int nPos = m_strAnswer.lastIndexOf(QString::fromLocal8Bit("。"));
//        if( nPos < 1 ){
//            nPos = m_strAnswer.lastIndexOf(".");
//            if( nPos < 1 ){
//                nPos = m_strAnswer.lastIndexOf(QString::fromLocal8Bit("，"));
//                if( nPos < 1 ){
//                    nPos = m_strAnswer.lastIndexOf(",");
//                    if( nPos < 1 ){
//                        nPos = m_strAnswer.lastIndexOf(" ");
//                    }
//                }
//            }
//        }
//        m_strAnswer = m_strAnswer.left(nPos+1);
//    }
    qDebug()<<"strAnswer::1"<<m_strAnswer;
    qDebug()<<"strAnswer::2"<<m_strAnswer.toStdString().data();

    if(m_bTTSIsUseKdxf){
        g_pMediaCtrl->media_play_tts(m_strAnswer);
        emit sig_show(2,m_strAnswer);
        m_strAnswer = "";
        return;
    }
    handshake();


}

void TmSerialWork::play_tts(const QString &strTts)
{
    QString strQuery = QString("{\"type\": \"tts\",\"content\": {\"action\": \"start\",\"text\": \"%1\"}}").arg(strTts);
    QByteArray sendData;
    if(createMsg(0x05,strQuery,sendData))
        slot_writeSerialData(sendData);
}

QString TmSerialWork::parser_tuling_json(QByteArray &bArry)
{
    QJsonDocument jsDoc;
    QJsonParseError error;
    jsDoc = QJsonDocument::fromJson(bArry, &error);
    if( error.error != QJsonParseError::NoError ){
        return "你欺负我，小薇不喜欢你了！ ";
    }
    QJsonObject jsObj = jsDoc.object();
    if(m_bIsReplyByTrio)
        return jsObj.value("reply").toString();

    QString strRet = "";
    int nCode = jsObj.value("code").toInt();
    switch (nCode) {
    case 100000:
        {
            strRet = jsObj.value("text").toString();
        }
        break;
    case 200000:
        {
            strRet = jsObj.value("text").toString()+jsObj.value("url").toString();
        }
        break;
    case 302000:
        {
            QJsonArray jsArry = jsObj.value("list").toArray();
            int nCur = g_nGetRandom(0, jsArry.size()-1);
            strRet = QString::fromLocal8Bit("小薇已经精心为你挑选了一条新闻:")+jsArry.at(nCur).toObject().value("article").toString();
        }
        break;
    case 308000:
        {
            QJsonObject oneitem = jsObj.value("list").toArray().at(0).toObject();
            QString strInfo = QString::fromLocal8Bit("%1%2%3").arg(oneitem.value("name").toString()).arg(QString::fromLocal8Bit("的菜谱为"))
                    .arg(oneitem.value("info").toString());
            strRet = QString::fromLocal8Bit("让小薇告诉你菜谱吧:")+strInfo;
        }
        break;
    case 313000:
        {
            QJsonObject oneitem = jsObj.value("function").toObject();
            QString strInfo = QString("歌名:%1,歌手:%2").arg(oneitem.value("song").toString())
                    .arg(oneitem.value("singer").toString());
            strRet = QString::fromLocal8Bit("等我学会了再给您唱好吗。");
        }
        break;
    case 314000:
        {
            QJsonObject oneitem = jsObj.value("function").toObject();
            QString strInfo = QString("诗名:%1,作者:%2").arg(oneitem.value("name").toString())
                    .arg(oneitem.value("author").toString());
            strRet = QString::fromLocal8Bit("这种简单的事情你自己来就好啦。");
        }
        break;
    default:
        strRet = QString::fromLocal8Bit("亲爱的,小薇知错啦，我会更加努力学习,跟上您的步伐的.");
        break;
    }
    if( nCode>40000 && nCode<50000 ){
        QString strOut = QString("tuling error! code:%1").arg(nCode);
//        g_log(strOut);
    }
    return strRet;
}
int TmSerialWork::g_nGetRandom(int nMin, int nMax)
{
    if( (nMax-nMin) == 0 ){
        return nMin;
    }else if(nMin > nMax){
        return nMax;
    }
    qsrand(QTime(0,0,0).msecsTo(QTime::currentTime()));
    //int n = (qrand()%((nMax-nMin+1)*100)) / 100 + nMin;
    int n = qrand()%(nMax-nMin+1) + nMin;
    return n;
}

void TmSerialWork::slot_writeSerialData(QByteArray bArry)
{
    if( !m_bInit ){
        return;
    }
    m_lock.lock();
    m_pSer->write(bArry); // real write data
    m_lock.unlock();
}
