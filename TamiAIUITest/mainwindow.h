#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QSettings>
#include <QThread>
#include "tmserialwork.h"
#include "mgzip.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();

    char valueToHexCh(const int value);

signals:
    void sig_initSerial(QString strCom);
    void sig_sendSerialData(QByteArray bArry);
    void sig_handshake();
    void sig_play_tts(const QString &t);
    void sig_recordVoice(int nTimeS);


private slots:
    void slot_show(int type,QString str);
    void on_Btn_conn_clicked();

    void on_Btn_reg_clicked();

    void on_pushButton_3_clicked();

//    void on_pushButton_5_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_record_clicked();

    void slot_setTTSPlayByKdxf();
    void slot_setTTSPlayByAIUI();
    void slot_clearEdit();

    void slot_setReplyByTrio();

    void slot_setReplyByTuLing();

private:
    Ui::MainWindow *ui;
    TmSerialWork *m_prSerial;
    char dataBuff[11];
    QSettings* g_pSettingsIni;
    QString strCom;
    void showLog(const QString &strLog);
};

#endif // MAINWINDOW_H
