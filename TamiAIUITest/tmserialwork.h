#ifndef TMSERIALWORK_H
#define TMSERIALWORK_H

#include <QObject>
#include <QSerialPort>
#include <QMutex>
#include <QQueue>
#include <QNetworkAccessManager>
#include <QNetworkReply>


#include "ctmmediactrl.h"

enum Type_Analysis{Type_Analysis_Err,Type_Analysis_Sccess};

class TmSerialWork : public QObject
{
    Q_OBJECT
public:
    explicit TmSerialWork(QObject *parent = 0);

    static void output(const QByteArray &b);
    int g_nGetRandom(int nMin, int nMax);

    bool countBuffLength(QByteArray &bData);
    static uchar InternalCheckCode(QByteArray buff);

    bool createMsg(uchar msgtype, const QString &msg, QByteArray &dataOut);
    bool createMsg(uchar msgtype, const QByteArray &msg, QByteArray &dataOut);

    void getWakeUpTTS();

    void setTTSUseBy(bool m_bIsUseKdxf);

    bool m_bIsReplyByTrio;
signals:
    void sig_show(int type,QString str);

public slots:
    void play_tts(const QString &strTts);
    void handshake();
    void slot_initSerial(QString strCom);

    void slot_recordVoice(int nTimeS);

    Type_Analysis slot_readSerialData();

    void slot_writeSerialData(QByteArray bArry);

    // tuling
    void tuling_replyFinish(QNetworkReply* reply);
    QString parser_tuling_json(QByteArray& bArry);



protected:
    bool verify(const QByteArray &data);
private:
    QSerialPort* m_pSer;
    QByteArray m_serBuff;
    QMutex m_lock;
    bool m_bInit;
    QQueue<QByteArray> m_ctrlList;
    QString m_strAnswer;
    QString m_strWakeUp;
    bool bIsWakeUp;
    QNetworkAccessManager* pr_httpGet;

    void prase(const QByteArray &data);
    int getAsrResult(const QString &data, QString &result);//获取语音识别结果   
    QByteArray confirm(uchar IdLow, uchar IdHigh);
    bool getJsonString(const QJsonObject &sourceObj, const QString &key, QString &destStr);
    bool getJsonObj(const QJsonObject &sourceObj, const QString &key, QJsonObject &destObj);

    CTMMediaCtrl *g_pMediaCtrl;
    bool m_bTTSIsUseKdxf;
};

#endif // TMSERIALWORK_H
