#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString g_strRootDir = QCoreApplication::applicationDirPath();
    QDir::setCurrent(g_strRootDir);
    g_pSettingsIni = new QSettings("./Config/Config.ini", QSettings::IniFormat);
    g_pSettingsIni->setIniCodec("UTF-8");
    strCom = g_pSettingsIni->value("Robot/COM","COM2").toString();
    qDebug()<<"strCom:"<<strCom;
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{    
    m_prSerial = new TmSerialWork();
    QThread *myThread = new QThread();
    m_prSerial->moveToThread(myThread);
    connect(this,SIGNAL(sig_initSerial(QString)),m_prSerial,SLOT(slot_initSerial(QString)));
    connect(this,SIGNAL(sig_sendSerialData(QByteArray)),m_prSerial,SLOT(slot_writeSerialData(QByteArray)),Qt::QueuedConnection);
    connect(this,SIGNAL(sig_handshake()),m_prSerial,SLOT(handshake()));
    connect(this,SIGNAL(sig_play_tts(QString)),m_prSerial,SLOT(play_tts(QString)));
    connect(m_prSerial,SIGNAL(sig_show(int,QString)),this,SLOT(slot_show(int,QString)));
    connect(this,SIGNAL(sig_recordVoice(int)),m_prSerial,SLOT(slot_recordVoice(int)));
    myThread->start();
    emit sig_initSerial(strCom);

    ui->tabWidget->tabBar()->hide();
    ui->tabWidget->setCurrentIndex(0);

    ui->actionTTsByAIUI->setChecked(true);
    connect(ui->actionTTSByKdxf,SIGNAL(triggered(bool)),this,SLOT(slot_setTTSPlayByKdxf()));
    connect(ui->actionTTsByAIUI,SIGNAL(triggered(bool)),this,SLOT(slot_setTTSPlayByAIUI()));
    connect(ui->clearAction,SIGNAL(triggered(bool)),this,SLOT(slot_clearEdit()));

    ui->actionTrio->setChecked(true);
    connect(ui->actionTrio,SIGNAL(triggered(bool)),this,SLOT(slot_setReplyByTrio()));
    connect(ui->actionTuLing,SIGNAL(triggered(bool)),this,SLOT(slot_setReplyByTuLing()));
}


char MainWindow::valueToHexCh(const int value)
{
    char result = '\0';
    if(value >= 0 && value <= 9){
        result = (char)(value + 48); //48为ascii编码的‘0’字符编码值
    }
    else if(value >= 10 && value <= 15){
        result = (char)(value - 10 + 65); //减去10则找出其在16进制的偏移量，65为ascii的'A'的字符编码值
    }
    else{
        ;
    }

    return result;
}

void MainWindow::slot_show(int type, QString str)
{
    if(type == 1){
        showLog("ASR result:"+str);
    }else{
        showLog("Answer:"+str);
    }
}

void MainWindow::showLog(const QString &strLog)
{
    QString s((strLog.size()>100 ? strLog.left(100)+"......" : strLog));
    static int n = 0;
    ui->textEdit->append(QTime::currentTime().toString("hh:mm:ss.zzz") + "  " + s);
    if(++n % 2 == 0){
        ui->textEdit->append("");
    }
}

void MainWindow::on_Btn_conn_clicked()
{
    QString strWifiName = ui->lineEdit_wifiName->text();
    QString strWifiPwd = ui->lineEdit_wifiPwd->text();

//    QByteArray wifiNameData = countBuffLength(strWifiName.toUtf8().toHex());
//    QByteArray wifiPwdData = countBuffLength(strWifiPwd.toUtf8().toHex());
//    qDebug()<<"wifiNameBuff"<<wifiNameBuff<<"wifiPwdBuff:"<<wifiPwdBuff;


//    char buffWifi[] = {0xA5, 0x01, 0x02,0x1B,0x00,0x02,0x00,0x00,0x02,0x08,0x0F,0x74,0x61,0x6d,0x69,0x31,0x37,0x30,0x33,0x74,0x61,0x6d,0x69,0x67,0x72,0x6f,0x75,0x70,0x40,0x31,0x37,0x23,0x30,0x33};
//    QByteArray bData(buffWifi,sizeof(buffWifi));
//    qDebug()<<"wify InternalCheckCode:"<<(int)InternalCheckCode(bData);
//    bData.append(InternalCheckCode(bData));
//    emit sig_sendSerialData(bData);

    uchar c[] = {0x00,0x02,0xFF,0xFF};
    QByteArray msgData((const char*)c,4);
    QByteArray bWifiName = strWifiName.toUtf8();
    QByteArray bWifiPwd = strWifiPwd.toUtf8();
    msgData.append(bWifiName);
    msgData.append(bWifiPwd);
    msgData[2] = bWifiName.size();
    msgData[3] = bWifiPwd.size();

//    QByteArray sendData;
//    if(createMsg(0x02,msgData,sendData))
//        emit sig_sendSerialData(sendData);
}



void MainWindow::on_Btn_reg_clicked()
{
    QString strAppid = ui->lineEdit_appid->text();
    QString strKey = ui->lineEdit_key->text();
    QString strData = QString("{\"type\": \"aiui_cfg\",\"content\": {\"appid\": \"%1\",\"key\": \"%2\", \"scene\":\"main\", \"launch_demo\":false}}").arg(strAppid).arg(strKey);

//    uchar b[] = {0xA5, 0x01, 0x03, 0x00, 0x00,0x04,0x00};
//    QByteArray bss((const char*)b,sizeof(b));
//    bss.append(strData.toUtf8());
//    if(!countBuffLength(bss))
//    {
//        QMessageBox::about(0,"err","长度设置错误");
//        return;
//    }
//    bss.append(InternalCheckCode(bss));

//    emit sig_sendSerialData(bss);

//    QByteArray sendData;
//    if(createMsg(0x03,strData,sendData))
//        emit sig_sendSerialData(sendData);
}


void MainWindow::on_pushButton_3_clicked()
{
    emit sig_play_tts(ui->lineEdit_2->text());
}

void MainWindow::on_pushButton_4_clicked()
{
    emit sig_handshake();
}

//void MainWindow::on_pushButton_5_clicked()
//{
//    uchar c[] = {0xf5, 0x00};
//    QByteArray m_serBuff((const char*)c,2);
//    uint n = ((m_serBuff[1]<<8)&0XFF00);
//    uint msgSize = (n|(m_serBuff[0] & 0xFF)) & 0XFFFF;//消息长度
//    qDebug()<<msgSize;
//}


void MainWindow::on_pushButton_record_clicked()
{
    emit sig_recordVoice(ui->spinBox->value());
}

void MainWindow::slot_setTTSPlayByKdxf()
{
    ui->actionTTSByKdxf->setChecked(true);
    ui->actionTTsByAIUI->setChecked(false);
    m_prSerial->setTTSUseBy(true);
}

void MainWindow::slot_setTTSPlayByAIUI()
{
    ui->actionTTsByAIUI->setChecked(true);
    ui->actionTTSByKdxf->setChecked(false);
    m_prSerial->setTTSUseBy(false);
}

void MainWindow::slot_clearEdit()
{
    ui->textEdit->clear();
}

void MainWindow::slot_setReplyByTrio()
{
    ui->actionTrio->setChecked(true);
    ui->actionTuLing->setChecked(false);
    m_prSerial->m_bIsReplyByTrio = true;
}

void MainWindow::slot_setReplyByTuLing()
{
    ui->actionTrio->setChecked(false);
    ui->actionTuLing->setChecked(true);
    m_prSerial->m_bIsReplyByTrio = false;
}
