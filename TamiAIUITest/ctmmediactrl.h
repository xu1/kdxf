#ifndef CTMMediaCtrl_H
#define CTMMediaCtrl_H

#include <QObject>
#include <QDir>
#include <QTextCodec>
#include <QDebug>
#include "libkdxf/qtts.h"
#include "libkdxf/qisr.h"
#include "libkdxf/msp_cmn.h"
#include "libkdxf/msp_errors.h"
#include <QThread>
#include <QFile>
#include <QMediaPlayer>
#include "libkdxf/sharedlib_kdxf.h"

//typedef int SR_DWORD;
//typedef short int SR_WORD ;
//struct wave_pcm_hdr
//{
//    char            riff[4];                        // = "RIFF"
//    SR_DWORD        size_8;                         // = FileSize - 8
//    char            wave[4];                        // = "WAVE"
//    char            fmt[4];                         // = "fmt "
//    SR_DWORD        dwFmtSize;                      // = 下一个结构体的大小 : 16

//    SR_WORD         format_tag;              // = PCM : 1
//    SR_WORD         channels;                       // = 通道数 : 1
//    SR_DWORD        samples_per_sec;        // = 采样率 : 8000 | 6000 | 11025 | 16000
//    SR_DWORD        avg_bytes_per_sec;      // = 每秒字节数 : dwSamplesPerSec * wBitsPerSample / 8
//    SR_WORD         block_align;            // = 每采样点字节数 : wBitsPerSample / 8
//    SR_WORD         bits_per_sample;         // = 量化比特数: 8 | 16

//    char            data[4];                        // = "data";
//    SR_DWORD        data_size;                // = 纯数据长度 : FileSize - 44
//} ;

class CTMMediaWorker;

class CTMMediaCtrl : public QObject
{
    Q_OBJECT
public:
    explicit CTMMediaCtrl(QObject *parent = 0);
    void init();
    void unInit();
    void media_play(QString strPath);
    void media_stop();
    void media_set_volume(int nVal);
    void media_play_tts(QString strTxt);
    void media_play_tts_params(QString strTxt, QString strSpeaker, int nSpeed, int nPitch, int nSpecial);
    bool media_is_playing();
    // bg media
    void media_bg_play(QString strPath);
    void media_bg_stop();
    void media_bg_set_volume(int nVal);
    bool media_bg_is_playing();

private:
    QThread* pr_pMediaThread;
    CTMMediaWorker* pr_pMediaWorker;
    QString pr_tts_speaker;
    int pr_tts_speed;
    int pr_tts_pitch;
    int pr_tts_special;
signals:
    void sig_init();
    void sig_unInit();
    void sig_media_play(QString strPath, int nIndex);
    void sig_media_stop(int nIndex);
    void sig_media_set_volume(int nVal, int nIndex);
    bool sig_media_play_tts(QString strTxt, QString strSpeaker, int nSpeed, int nPitch, int nSpecial);
public slots:
};

class CTMMediaWorker : public QObject
{
    Q_OBJECT
public:
    explicit CTMMediaWorker(QObject *parent = 0);

    bool getIsPlaying(int nIndex=0);

    Sharedlib_kdxf* pr_pAsrSDK;
    QString g_strRootDir;
private:
    QMediaPlayer* pr_pMediaPlayer;
    QMediaPlayer* pr_pMediaPlayerBG;
    QString pr_strRootDir;
    QTextCodec* pr_pTextCodec;

public slots:
    void slot_init();
    void slot_unInit();
    void slot_media_play(QString strPath, int nIndex=0);
    void slot_media_stop(int nIndex=0);
    void slot_media_set_volume(int nVal, int nIndex=0);
    bool slot_media_play_tts(QString strTxt, QString strSpeaker, int nSpeed, int nPitch, int nSpecial);

    void slot_state_change(QMediaPlayer::State state);
};

#endif // CTMMediaCtrl_H
