﻿#ifndef MGZIP_H
#define MGZIP_H
#include <QByteArray>
#include <QString>

namespace MGzip {

QByteArray mcompress(const QString &text, int max_compress_size);//压缩
QString muncompress(const QByteArray &zgzipData, int max_uncompress_size);//解压

}

#endif // MGZIP_H
