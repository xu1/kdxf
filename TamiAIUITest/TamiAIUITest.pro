#-------------------------------------------------
#
# Project created by QtCreator 2017-03-21T14:39:22
#
#-------------------------------------------------

QT       += core gui serialport widgets network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TamiAIUITest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tmserialwork.cpp \
    mgzip.cpp \
    ctmmediactrl.cpp

HEADERS  += mainwindow.h \
    tmserialwork.h \
    mgzip.h \
    ctmmediactrl.h

FORMS    += mainwindow.ui


# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lmsc

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf

# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lsharedlib_kdxf

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf
