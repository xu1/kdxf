/*
* Copyright(C), 2016-2016, Tami Group.
* File name:  CTMGLOBALCTRL_H
* Author:     xin.zhang
* Version:   R2.0
* Date:   2016-06-18 10:52:52
* Description: 全局共享变量和头文件
*/

#ifndef CTMGLOBALCTRL_H
#define CTMGLOBALCTRL_H
#include <QtCore>
#include <QDebug>
#include <QSettings>


#include "ctmaudioctrl.h"
#include "ctmasrtool.h"
#include "ctmmediactrl.h"
#include "ctmasranswer.h"

void g_clearDir(QString strDir);
int g_nGetRandom(int nMin, int nMax);
QString g_strGetMD5(QString strTxt);

extern QString g_strRootDir;
extern QSettings* g_pSettingsIni;
extern void* g_pZmqCtx;

extern CTMMediaCtrl* g_pCtrlMedia;
extern CTMAudioCtrl* g_pCtrlAudio;
extern CTmAsrTool* g_pAsrTool;
extern CTmAsrAnswer* g_pAsrAnswer;


#endif // CTMGLOBALCTRL_H
