#include "ctmasrtool.h"
#include "ctmglobalctrl.h"



CTmAsrTool::CTmAsrTool(QObject *parent) : QObject(parent)
{
    m_bEnable = false;
    m_bStop = false;
    bDebug = g_pSettingsIni->value("ASR/PCM", 0).toInt();
    QString strGramaName = g_pSettingsIni->value("ASR/GramaName","tami").toString();
    pr_pAsrSDK = new Sharedlib_kdxf();
    if(!pr_pAsrSDK->login("556d116e")){
        qDebug()<<"KDXF SDK login Failed!";
    }
    connect(pr_pAsrSDK,SIGNAL(sig_IsrResult(QString)),this,SIGNAL(sig_asr_over_online(QString)));
    connect(this, &CTmAsrTool::sig_get_result, this, &CTmAsrTool::slot_get_result);
    setGramaName(strGramaName);
    buildConfig(strGramaName);

}

void CTmAsrTool::enable(bool bIsOffline, bool bIsChinese)
{
    m_bEnable = true;
    pr_pAsrSDK->isr_start(bIsOffline, bIsChinese);
    pr_pAudioPCM = NULL;
    if( bDebug ){
        QString strTmp = QTime::currentTime().toString("hhmmsszzz")+".pcm";
        pr_pAudioPCM = fopen(strTmp.toLocal8Bit().data(), "wb");
    }
}

void CTmAsrTool::disable()
{
    m_bEnable = false;
    pr_pAsrSDK->isr_stop();
}

bool CTmAsrTool::buildConfig(QString strFileName)
{
    // build
    return pr_pAsrSDK->isr_build_bnf(strFileName);
}

void CTmAsrTool::setGramaName(QString strName)
{
    pr_pAsrSDK->isr_setGramID(strName);
}
//FILE* pTest = fopen("asr.pcm","wb");
void CTmAsrTool::slot_audio_data_in(QByteArray bArray)
{
    if(m_bEnable)
    {
        if( m_bStop ){
            m_bStop = false;
            emit sig_get_result();
            if( pr_pAudioPCM ){
                fclose(pr_pAudioPCM);
                pr_pAudioPCM = NULL;
            }
        }else{
            //qDebug()<<"isr_updateAudio:"<<bArray.size();
            pr_pAsrSDK->isr_updateAudio(bArray.data(), bArray.length());
            if(pr_pAudioPCM){
                fwrite(bArray.data(), 1, bArray.length(), pr_pAudioPCM);
            }
        }
    }
}

void CTmAsrTool::slot_stop()
{
    if(m_bEnable)
        m_bStop = true;
}

void CTmAsrTool::slot_get_result()
{
    QString strResultXML = pr_pAsrSDK->isr_getResult();
    pr_pAsrSDK->isr_stop();

    int nConfidence = 0;
    QString strResultWord = "";

#if 1
    if( strResultXML.length()>0 ){
        QDomDocument xmlDoc;
        QString strError;
        int nErrorLine, nErrorColum;
        if( !xmlDoc.setContent(strResultXML, &strError, &nErrorLine, &nErrorColum) ){
            qDebug()<< QString("Xml Parse Error! [%1:%2] %3").arg(nErrorLine).arg(nErrorColum).arg(strError);
            return;
        }
        // pase obj
        QDomElement root = xmlDoc.documentElement();
        if( root.isNull() ){
            qDebug()<<"xml Root is NULL!";
            return;
        }

        QDomNodeList nodelist = root.elementsByTagName("confidence");
        if( nodelist.length()<1 ){
            qDebug()<<"Nodelist is null";
            return;
        }
        QString strConfidence = nodelist.at(0).toElement().text();
        nConfidence = strConfidence.toInt(); // 返回可信度
        // result
        nodelist = root.elementsByTagName("result");
        if( nodelist.length()>0 ){
            nodelist = nodelist.at(0).toElement().elementsByTagName("object");
            if( nodelist.length()>0 ){
                QDomNodeList objList = nodelist.at(0).toElement().childNodes();
                QDomElement oneEle;
                for(int i=0; i<objList.size();i++){
                    oneEle = objList.at(i).toElement();
                    if( oneEle.tagName() == "ask" ){
                        strResultWord = oneEle.text();
                        break;
                    }
                }
            }
        }

    }
    qDebug()<<"================================"<<nConfidence<<strResultWord;
     emit sig_asr_over(strResultWord);
#endif
}


