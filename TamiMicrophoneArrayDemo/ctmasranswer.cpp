﻿#include "ctmasranswer.h"
#include "ctmglobalctrl.h"

CTmAsrAnswer::CTmAsrAnswer(QObject *parent) : QObject(parent)
{
    m_bIsReplyByTrio = false;
    pr_httpGet = new QNetworkAccessManager(this);
    connect(pr_httpGet,SIGNAL(finished(QNetworkReply*)),this,SLOT(tuling_replyFinish(QNetworkReply*)));
}

void CTmAsrAnswer::tuling_replyFinish(QNetworkReply *reply)
{
    QByteArray bReply =reply->readAll();
    QString m_strAnswer = parser_tuling_json(bReply);
    qDebug()<<"bReply:"<<bReply;
//    if( m_strAnswer.length() > 100 ){
//        m_strAnswer = m_strAnswer.left(100);
//        int nPos = m_strAnswer.lastIndexOf("。");
//        if( nPos < 1 ){
//            nPos = m_strAnswer.lastIndexOf(".");
//            if( nPos < 1 ){
//                nPos = m_strAnswer.lastIndexOf("，");
//                if( nPos < 1 ){
//                    nPos = m_strAnswer.lastIndexOf(",");
//                    if( nPos < 1 ){
//                        nPos = m_strAnswer.lastIndexOf(" ");
//                    }
//                }
//            }
//        }
//        m_strAnswer = m_strAnswer.left(nPos+1);
//    }

    sig_replyResult(m_strAnswer);
    qDebug()<<"strAnswer::"<<m_strAnswer;
}

QString CTmAsrAnswer::parser_tuling_json(QByteArray &bArry)
{
    QJsonDocument jsDoc;
    QJsonParseError error;
    jsDoc = QJsonDocument::fromJson(bArry, &error);
    if( error.error != QJsonParseError::NoError ){
        return "你欺负我，小薇不喜欢你了！ ";
    }
    QJsonObject jsObj = jsDoc.object();
    if(m_bIsReplyByTrio)
        return jsObj.value("reply").toString();


    QString strRet = "";
    int nCode = jsObj.value("code").toInt();    
    switch (nCode) {
    case 0:
        {
            QJsonValue jsVal = jsObj.value("payload");
            QJsonObject jsTextObj = jsVal.toObject();
            strRet = jsTextObj.value("text").toString();
        }
        break;
    case 100000:
        {
            strRet = jsObj.value("text").toString();
        }
        break;
    case 200000:
        {
            strRet = jsObj.value("text").toString()+jsObj.value("url").toString();
        }
        break;
    case 302000:
        {
            QJsonArray jsArry = jsObj.value("list").toArray();
            int nCur = g_nGetRandom(0, jsArry.size()-1);
            strRet = QString::fromLocal8Bit("小薇已经精心为你挑选了一条新闻:")+jsArry.at(nCur).toObject().value("article").toString();
        }
        break;
    case 308000:
        {
            QJsonObject oneitem = jsObj.value("list").toArray().at(0).toObject();
            QString strInfo = QString("%1%2%3").arg(oneitem.value("name").toString()).arg(QString::fromLocal8Bit("的菜谱为"))
                    .arg(oneitem.value("info").toString());
            strRet = QString::fromLocal8Bit("让小薇告诉你菜谱吧,")+strInfo;
        }
        break;
    case 313000:
        {
            QJsonObject oneitem = jsObj.value("function").toObject();
            QString strInfo = QString("歌名:%1,歌手:%2").arg(oneitem.value("song").toString())
                    .arg(oneitem.value("singer").toString());
            strRet = QString::fromLocal8Bit("等我学会了再给您唱好吗。");
        }
        break;
    case 314000:
        {
            QJsonObject oneitem = jsObj.value("function").toObject();
            QString strInfo = QString("诗名:%1,作者:%2").arg(oneitem.value("name").toString())
                    .arg(oneitem.value("author").toString());
            strRet = QString::fromLocal8Bit("这种简单的事情你自己来就好啦。");
        }
        break;
    default:
        strRet = QString::fromLocal8Bit("亲爱的,小薇知错啦，我会更加努力学习,跟上您的步伐的。");
        break;
    }
    if( nCode>40000 && nCode<50000 && nCode == 1){
        QString strOut = QString("tuling error! code:%1").arg(nCode);
        qDebug()<<strOut;
    }
    return strRet;
}

void CTmAsrAnswer::slot_getAnswer(bool bIsChinese,QString strQuestion)
{
    QString str = strQuestion.trimmed();
    QString strUrl;
    if(m_bIsReplyByTrio){
        strUrl = QString("http://sandbox.sanjiaoshou.net/Api/chat?who=tami&userID=%1&sentence=%2")
            .arg("tami_111").arg(strQuestion);
    }else{
        if(bIsChinese){
            strUrl = QString("http://www.tuling123.com/openapi/api?key=%1&userid=%2&info=%3")
                            .arg("652f89bd9d9248f99e46d96d5b649228").arg("abc123").arg(str);
        }else{
            strUrl = QString("http://smartdevice.ai.tuling123.com/en/conversation?apikey=%1&userid=%2&text=%3")
                            .arg("b1833040534a6bfd761215154069ea58").arg("111").arg(str);
        }
    }

    qDebug()<<"bIsChinese"<<bIsChinese<<strUrl;
    pr_httpGet->get(QNetworkRequest(QUrl(strUrl)));
}

