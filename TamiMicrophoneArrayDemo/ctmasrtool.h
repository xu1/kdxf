#ifndef CTMASRTOOL
#define CTMASRTOOL

#include <QObject>
#include <QtCore>
#include <QDomDocument>
#include "libkdxf/sharedlib_kdxf.h"


class CTmAsrTool : public QObject
{
    Q_OBJECT
public:
    explicit CTmAsrTool(QObject *parent = 0);
    //开始语音识别
    void enable(bool bIsOffline, bool bIsChinese);
    //停止语音识别
    void disable();
    //编译语法文件
    bool buildConfig(QString strFileName);
    //设置语法名字
    void setGramaName(QString strName);

    Sharedlib_kdxf* pr_pAsrSDK;
private:
    //QTime pr_nTimeEnd, pr_nTimeStart;
    FILE* pr_pAudioPCM;
    bool bDebug;
    bool m_bStop;
    bool m_bEnable;

    const char* session_id;

signals:
    void sig_asr_over_online(QString strResult);
    void sig_asr_over(QString strResult);
    //获取识别结果信号
    void sig_get_result();
public slots:
    //获取音频数据并创建音频文件
    void slot_audio_data_in(QByteArray bArray);
    //获取识别结果
    void slot_get_result();

    //
    void slot_stop();
};

#endif // CTMASRTOOL

