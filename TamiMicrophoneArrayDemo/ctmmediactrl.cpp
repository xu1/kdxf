#include "ctmmediactrl.h"
#include "ctmglobalctrl.h"

CTMMediaCtrl::CTMMediaCtrl(QObject *parent) : QObject(parent)
{
    pr_pMediaThread = NULL;
    pr_pMediaWorker = NULL;
}

void CTMMediaCtrl::init()
{
    if( pr_pMediaWorker || pr_pMediaThread ){
        return;
    }
    pr_pMediaThread = new QThread();
    pr_pMediaWorker = new CTMMediaWorker();

    connect(this, SIGNAL(sig_init()), pr_pMediaWorker, SLOT(slot_init()));
    connect(this, SIGNAL(sig_unInit()), pr_pMediaWorker, SLOT(slot_unInit()));
    connect(this, SIGNAL(sig_media_play(QString, int)), pr_pMediaWorker, SLOT(slot_media_play(QString, int)));
    connect(this, SIGNAL(sig_media_stop(int)), pr_pMediaWorker, SLOT(slot_media_stop(int)));
    connect(this, SIGNAL(sig_media_play_tts(QString,QString,int,int,int)), pr_pMediaWorker, SLOT(slot_media_play_tts(QString,QString,int,int,int)));
    connect(this, SIGNAL(sig_media_set_volume(int,int)), pr_pMediaWorker, SLOT(slot_media_set_volume(int,int)));
    pr_pMediaWorker->moveToThread(pr_pMediaThread);
    pr_pMediaThread->start();

    // ini config    
    pr_tts_speaker = g_pSettingsIni->value("TTS/Speaker", "nannan").toString();
    pr_tts_speed = g_pSettingsIni->value("TTS/Speed", 50).toInt();
    pr_tts_pitch = g_pSettingsIni->value("TTS/Pitch", 50).toInt();
    pr_tts_special = g_pSettingsIni->value("TTS/Special", 0).toInt();
    g_pAsrTool->pr_pAsrSDK->setDefaultParams(pr_tts_speaker,pr_tts_speed,pr_tts_pitch,pr_tts_special);

    emit sig_init();

}

void CTMMediaCtrl::unInit()
{
    if( pr_pMediaThread ){
        pr_pMediaThread->quit();
        pr_pMediaThread = NULL;
    }
    if( pr_pMediaWorker ){
        pr_pMediaWorker = NULL;
    }
    emit sig_unInit();
}

void CTMMediaCtrl::media_play(QString strPath)
{
    emit sig_media_play(strPath, 0);
}

void CTMMediaCtrl::media_stop()
{
    emit sig_media_stop(0);
}

void CTMMediaCtrl::media_set_volume(int nVal)
{
    emit sig_media_set_volume(nVal, 0);
}

void CTMMediaCtrl::media_play_tts(QString strTxt)
{
    emit sig_media_play_tts(strTxt, pr_tts_speaker, pr_tts_speed, pr_tts_pitch, pr_tts_special);
}

void CTMMediaCtrl::media_play_tts_params(QString strTxt, QString strSpeaker, int nSpeed, int nPitch, int nSpecial)
{
    emit sig_media_play_tts(strTxt, strSpeaker, nSpeed, nPitch, nSpecial);
}

bool CTMMediaCtrl::media_is_playing()
{
    if( pr_pMediaWorker ){
        return pr_pMediaWorker->getIsPlaying(0);
    }
    return false;
}

void CTMMediaCtrl::media_bg_play(QString strPath)
{
    emit sig_media_play(strPath,1);
}

void CTMMediaCtrl::media_bg_stop()
{
    emit sig_media_stop(1);
}

void CTMMediaCtrl::media_bg_set_volume(int nVal)
{
    emit sig_media_set_volume(nVal, 1);
}

bool CTMMediaCtrl::media_bg_is_playing()
{
    if( pr_pMediaWorker ){
        return pr_pMediaWorker->getIsPlaying(1);
    }
    return false;
}

CTMMediaWorker::CTMMediaWorker(QObject *parent)
{
    Q_UNUSED(parent);
    pr_pMediaPlayer = NULL;
    pr_pTextCodec = NULL;
    pr_strRootDir = QString("%1/Data/TTS/").arg(g_strRootDir);
    pr_pMediaPlayerBG = NULL;
}

bool CTMMediaWorker::getIsPlaying(int nIndex)
{
    if( pr_pMediaPlayer && nIndex == 0){
        return (QMediaPlayer::PlayingState==pr_pMediaPlayer->state());
    }
    if( pr_pMediaPlayerBG && nIndex == 1){
        return (QMediaPlayer::PlayingState==pr_pMediaPlayerBG->state());
    }
    return false;
}

void CTMMediaWorker::slot_init()
{
    pr_pMediaPlayer = new QMediaPlayer();
    connect(pr_pMediaPlayer, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(slot_state_change(QMediaPlayer::State)));
    // 初始音量设置
    int nVolume = g_pSettingsIni->value("Robot/Volume", 60).toInt();
    pr_pMediaPlayer->setVolume(nVolume);

    // BG
    pr_pMediaPlayerBG = new QMediaPlayer();
    pr_pMediaPlayerBG->setVolume(nVolume);

    // make path
    pr_strRootDir = QString("%1/Data/TTS/").arg(g_strRootDir);
    QDir dirRoot(pr_strRootDir);
    if( !dirRoot.exists() ){
        if(!dirRoot.mkpath(pr_strRootDir)){
//            printf("CTMMediaCtrl: make TTS path Failed!\n");
//            emit g_pDlgMain->sig_showLog(QString("CTMMediaCtrl: make TTS path Failed!"));
        }
    }
    // init kdxf lib
    pr_pTextCodec = QTextCodec::codecForName("GB18030");

}

void CTMMediaWorker::slot_unInit()
{
    g_pAsrTool->pr_pAsrSDK->logout();
}

void CTMMediaWorker::slot_media_play(QString strPath, int nIndex)
{
    if( strPath.length() < 1 ){
        return;
    }
    QDir dirCur(strPath);
    strPath = dirCur.absolutePath();
    if( nIndex == 0 && pr_pMediaPlayer ){
        pr_pMediaPlayer->setMedia(QUrl::fromLocalFile(strPath));
        pr_pMediaPlayer->play();
    }
    if( nIndex == 1 && pr_pMediaPlayerBG ){
        pr_pMediaPlayerBG->setMedia(QUrl::fromLocalFile(strPath));
        pr_pMediaPlayerBG->play();
    }
}

void CTMMediaWorker::slot_media_stop(int nIndex)
{
    if( pr_pMediaPlayer && nIndex == 0 ){
        pr_pMediaPlayer->stop();
    }
    if( pr_pMediaPlayerBG && nIndex == 1 ){
        pr_pMediaPlayerBG->stop();
    }
}

void CTMMediaWorker::slot_media_set_volume(int nVal, int nIndex)
{
    if( pr_pMediaPlayer && nIndex == 0 ){
        pr_pMediaPlayer->setVolume(nVal);
    }
    if( pr_pMediaPlayerBG && nIndex == 1 ){
        pr_pMediaPlayerBG->setVolume(nVal);
    }
}

bool CTMMediaWorker::slot_media_play_tts(QString strTxt, QString strSpeaker, int nSpeed, int nPitch, int nSpecial)
{
    if( !g_pAsrTool ){
//        printf("TTS kdxf init Failed!\n");
//        emit g_pDlgMain->sig_showLog(QString("TTS kdxf init Failed!"));
        return false;
    }
    QString strParams = QString("%1_%2_%3_%4_").arg(strSpeaker).arg(nSpeed).arg(nPitch).arg(nSpecial);
    QString strMd5Path = QString("%1%2.wav").arg(pr_strRootDir).arg(g_strGetMD5(strParams+strTxt));
    //qDebug()<<strMd5Path;
    QFileInfo fi(strMd5Path);
    if( fi.isFile() ){
        slot_media_play(strMd5Path);
        return true;
    }
    // no file, create it
//    qDebug()<<"语音参数==yuyincanshu："<<strSpeaker<<nSpeed<<nPitch<<nSpecial;
    int nLen = g_pAsrTool->pr_pAsrSDK->getTTSfile(strTxt, strMd5Path, strSpeaker, nSpeed, nPitch, nSpecial);
//    int nLen = _tts_make_file(strTxt, strMd5Path, strSpeaker, nSpeed, nPitch, nSpecial);
    if( nLen < 46 ){
        QFile f(strMd5Path);
        f.remove();
//        printf("CTMMediaWorker: tts make file failed! size: %d\n", nLen);
//        emit g_pDlgMain->sig_showLog(QString("CTMMediaWorker: tts make file failed! size: %1").arg(nLen));
//        g_pLog->log_E("[TTS] tts make file failed!");
        return false;
    }
    // play
    slot_media_play(strMd5Path);
    return true;
}

void CTMMediaWorker::slot_state_change(QMediaPlayer::State state)
{
    if( state == QMediaPlayer::StoppedState ){
        QByteArray strJson = QString("{ \"type\":\"notify\",\"target\":\"media\", \"data\":{\"state\":\"%1\"} }").arg("stopped").toUtf8();
//        g_pCtrlNotify->sendNotify(strJson);
    }
}

