#-------------------------------------------------
#
# Project created by QtCreator 2017-03-27T09:22:57
#
#-------------------------------------------------

QT       += core gui xml multimedia network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = TamiMicrophoneArrayDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ctmasrtool.cpp \
    ctmaudioctrl.cpp \
    ctmglobalctrl.cpp \
    ctmmediactrl.cpp \
    ctmasranswer.cpp

HEADERS  += mainwindow.h \
    ctmasrtool.h \
    ctmaudioctrl.h \
    ctmglobalctrl.h \
    ctmmediactrl.h \
    ctmasranswer.h

FORMS    += mainwindow.ui

# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lmsc

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf

# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lsharedlib_kdxf

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf
