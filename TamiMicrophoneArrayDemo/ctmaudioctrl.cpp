#include "ctmaudioctrl.h"
#include <QCoreApplication>
#include "ctmglobalctrl.h"

CTMAudioCtrl::CTMAudioCtrl(QObject *parent) : QObject(parent)
{
    pr_pAudioInThread = NULL;
    pr_pAudioOutThread = NULL;

}

void CTMAudioCtrl::init()
{
    if( !pr_pAudioInThread ){
        pr_pAudioInThread = new CTMAudioInThread();
        connect(pr_pAudioInThread,SIGNAL(sig_audioInStop()),this,SIGNAL(sig_audioInStop()));
        pr_pAudioInThread->start();

    }
    QThread::msleep(80);
    if( !pr_pAudioOutThread ){
        pr_pAudioOutThread = new CTMAudioOutThread();
        pr_pAudioOutThread->start();
        connect(pr_pAudioOutThread,SIGNAL(sig_ASR_AudioData(QByteArray)),
                g_pAsrTool,SLOT(slot_audio_data_in(QByteArray)),Qt::QueuedConnection);
    }
}

// Audio In
CTMAudioInThread::CTMAudioInThread()
{
    pr_bExit = false;
    pr_pAudioDeviceOut = NULL;
    pr_pAudioOutput = NULL;
}

void CTMAudioInThread::run()
{
   // init audio in
    if( !pr_pAudioOutput ){
        QAudioFormat format;
        // Set up the desired format, for example:
        format.setSampleRate(16000);
        format.setChannelCount(1);
        format.setSampleSize(16);
        format.setCodec("audio/pcm");
        format.setByteOrder(QAudioFormat::LittleEndian);
        format.setSampleType(QAudioFormat::SignedInt);
        QAudioDeviceInfo info = QAudioDeviceInfo::defaultOutputDevice();
        if (!info.isFormatSupported(format)) {
           //qWarning() << "Default format not supported, trying to use the nearest.";
           format = info.nearestFormat(format);
        }
        pr_pAudioOutput = new QAudioOutput(format);
        pr_pAudioDeviceOut = new CTMAudioOutputDevice();
        connect(pr_pAudioDeviceOut,SIGNAL(sig_audioInStop()),this,SIGNAL(sig_audioInStop()));
        pr_pAudioOutput->start(pr_pAudioDeviceOut);
 }
    // loop
    while(!pr_bExit){
        msleep(10);
        QCoreApplication::processEvents();

//        if( nLen > 0 ){
//            pr_pAudioDeviceOut->writeData(buff, nLen);
//        }

    }

    // close
    if( pr_pAudioOutput ){
        pr_pAudioOutput->stop();
        pr_pAudioOutput = NULL;
    }
    if( pr_pAudioDeviceOut ){
        pr_pAudioDeviceOut->close();
        pr_pAudioDeviceOut = NULL;
    }


}

void CTMAudioInThread::setExit()
{
    pr_bExit = true;
}

// Audio Out
CTMAudioOutThread::CTMAudioOutThread()
{
    pr_bExit = false;
    pr_pAudioDeviceIn = NULL;
    pr_pAudioInput = NULL;
}

void CTMAudioOutThread::run()
{

    // init audio out
    if( !pr_pAudioInput ){
        QAudioFormat format;
        // Set up the desired format, for example:
        format.setSampleRate(16000);
        format.setChannelCount(1);
        format.setSampleSize(16);
        format.setCodec("audio/pcm");
        format.setByteOrder(QAudioFormat::LittleEndian);
        format.setSampleType(QAudioFormat::SignedInt);
        QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
        if (!info.isFormatSupported(format)) {
           //qWarning() << "Default format not supported, trying to use the nearest.";
           format = info.nearestFormat(format);
        }
        pr_pAudioInput = new QAudioInput(format);
        pr_pAudioDeviceIn = pr_pAudioInput->start();
        connect(pr_pAudioDeviceIn, SIGNAL(readyRead()), this, SLOT(slot_audio_data()), Qt::QueuedConnection);
  }
    // loop
    while(!pr_bExit){
        msleep(10);
        QCoreApplication::processEvents();
    }

    // close
    if( pr_pAudioInput ){
        pr_pAudioInput->stop();
        pr_pAudioInput = NULL;
    }
    if( pr_pAudioDeviceIn ){
        pr_pAudioDeviceIn->close();
        pr_pAudioDeviceIn = NULL;
    }

}

void CTMAudioOutThread::setExit()
{
    pr_bExit = true;
}
void output(const QByteArray &b)
{
    QString str;
    foreach (uchar c, b) {
        str += QString::number((long)c,16) + " ";
    }
    qDebug()<<str;
}
void CTMAudioOutThread::slot_audio_data()
{
    if( pr_pAudioInput && pr_pAudioDeviceIn ){
        QByteArray bArray;
        bArray = pr_pAudioDeviceIn->readAll();

        //int ilen = pr_pAudioDeviceIn->bytesAvailable();
        //qDebug()<<"*****"<<ilen<<bArray.size();

//        static int n = 0;
//        if(n++%5==0)
//            output(bArray);
//        qDebug()<<"CTMAudioOutThread::slot_audio_data()"<<bArray.size();
        emit sig_ASR_AudioData(bArray);
    }
}
