#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ctmglobalctrl.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    g_strRootDir = QCoreApplication::applicationDirPath();
    QDir::setCurrent(g_strRootDir);
    g_pSettingsIni = new QSettings("./Config/Robot.ini", QSettings::IniFormat);
    g_pSettingsIni->setIniCodec("UTF-8");

    connect(ui->actionClear,SIGNAL(triggered(bool)),this,SLOT(slot_clearEdit()));
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    g_pAsrTool = new CTmAsrTool();
    connect(g_pAsrTool,SIGNAL(sig_asr_over_online(QString)),this,SLOT(slot_asr_result(QString)));
    connect(g_pAsrTool,SIGNAL(sig_asr_over(QString)),this,SLOT(slot_asr_result(QString)));


    g_pAsrAnswer = new CTmAsrAnswer();
    connect(g_pAsrAnswer,SIGNAL(sig_replyResult(QString)),this,SLOT(slot_getAnswer(QString)));

    QThread::msleep(80);
    g_pCtrlAudio = new CTMAudioCtrl();
    connect(g_pCtrlAudio,SIGNAL(sig_audioInStop()),g_pAsrTool,SLOT(slot_stop()));
    g_pCtrlAudio->init();

    QThread::msleep(100);
    if(!g_pCtrlMedia){
        g_pCtrlMedia = new CTMMediaCtrl();
        g_pCtrlMedia->init();
    }


    ui->actionTuLing->setChecked(true);
    connect(ui->actionTrio,SIGNAL(triggered(bool)),this,SLOT(slot_setReplyByTrio()));
    connect(ui->actionTuLing,SIGNAL(triggered(bool)),this,SLOT(slot_setReplyByTuLing()));

    connect(ui->actionOnline,SIGNAL(triggered(bool)),this,SLOT(slot_setIsOnline()));
    connect(ui->actionOffline,SIGNAL(triggered(bool)),this,SLOT(slot_setIsOffline()));
    connect(ui->actionChinese,SIGNAL(triggered(bool)),this,SLOT(slot_setChinese()));
    connect(ui->actionEnglish,SIGNAL(triggered(bool)),this,SLOT(slot_setEnglish()));

    g_pAsrTool->enable(false,true);
    ui->Btn_start->hide();
}

void MainWindow::slot_clearEdit()
{
    ui->textEdit->clear();
}

void MainWindow::slot_setReplyByTrio()
{
    ui->actionTrio->setChecked(true);
    ui->actionTuLing->setChecked(false);
    g_pAsrAnswer->m_bIsReplyByTrio = true;
}

void MainWindow::slot_setReplyByTuLing()
{
    ui->actionTrio->setChecked(false);
    ui->actionTuLing->setChecked(true);
    g_pAsrAnswer->m_bIsReplyByTrio = false;
}

void MainWindow::slot_setIsOnline()
{
    ui->actionOnline->setChecked(true);
    ui->actionOffline->setChecked(false);
    bIsOffline = false;
    bIsChinese = ui->actionChinese->isChecked();
    g_pAsrTool->disable();
    g_pAsrTool->enable(bIsOffline,bIsChinese);
}

void MainWindow::slot_setIsOffline()
{
    ui->actionOnline->setChecked(false);
    ui->actionOffline->setChecked(true);
    bIsOffline = true;
    bIsChinese = ui->actionChinese->isChecked();
    g_pAsrTool->disable();
    g_pAsrTool->enable(bIsOffline,bIsChinese);
}

void MainWindow::slot_setChinese()
{
    ui->actionEnglish->setChecked(false);
    ui->actionChinese->setChecked(true);
    bIsChinese = true;
    bIsOffline = ui->actionOffline->isChecked();
    g_pAsrTool->disable();
    g_pAsrTool->enable(bIsOffline,bIsChinese);
}

void MainWindow::slot_setEnglish()
{
    ui->actionEnglish->setChecked(true);
    ui->actionChinese->setChecked(false);
    bIsChinese = false;
    bIsOffline = ui->actionOffline->isChecked();
    g_pAsrTool->disable();
    g_pAsrTool->enable(bIsOffline,bIsChinese);
}

void MainWindow::slot_asr_result(QString strQuestion)
{
    qDebug()<<"asr_result"<<strQuestion.toStdString().data();
    if(strQuestion.size() <= 1)
        return;
    showLog("ASR result:"+strQuestion);    
    g_pAsrAnswer->slot_getAnswer(bIsChinese,strQuestion);
}

void MainWindow::slot_getAnswer(QString strAnswer)
{
    qDebug()<<"strAnswer"<<strAnswer.toStdString().data();
    showLog("Answer:" + strAnswer);
    g_pCtrlMedia->media_play_tts(strAnswer);
}

void MainWindow::showLog(const QString &strLog)
{
    QString s((strLog.size()>100 ? strLog.left(100)+"......" : strLog));
    static int n = 0;
    ui->textEdit->append(QTime::currentTime().toString("hh:mm:ss.zzz") + "  " + s);
    if(++n % 2 == 0){
        ui->textEdit->append("");
    }
}

void MainWindow::on_Btn_start_pressed()
{
    ui->Btn_start->setText(("松开识别"));
    g_pAsrTool->enable(bIsOffline,bIsChinese);
}

void MainWindow::on_Btn_start_released()
{
    ui->Btn_start->setText(("按下说话"));
    g_pAsrTool->slot_stop();
}
