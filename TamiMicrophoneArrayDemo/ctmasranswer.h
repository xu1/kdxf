#ifndef CTMASRANSWER_H
#define CTMASRANSWER_H

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class CTmAsrAnswer : public QObject
{
    Q_OBJECT
public:
    explicit CTmAsrAnswer(QObject *parent = 0);

    bool m_bIsReplyByTrio;
signals:
    void sig_replyResult(QString strAnswer);
public slots:
    // tuling
    void tuling_replyFinish(QNetworkReply* reply);
    QString parser_tuling_json(QByteArray& bArry);
    void slot_getAnswer(bool bIsChinese, QString strQuestion);

private:
    QNetworkAccessManager* pr_httpGet;


};

#endif // CTMASRANSWER_H
