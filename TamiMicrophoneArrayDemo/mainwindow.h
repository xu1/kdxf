#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();
private slots:
    void slot_asr_result(QString strQuestion);

    void slot_getAnswer(QString strAnswer);

    void slot_clearEdit();

    void slot_setReplyByTrio();

    void slot_setReplyByTuLing();

    void slot_setIsOnline();
    void slot_setIsOffline();
    void slot_setChinese();
    void slot_setEnglish();

    void on_Btn_start_pressed();

    void on_Btn_start_released();

private:
    Ui::MainWindow *ui;
    void showLog(const QString &strLog);

    bool bIsOffline;
    bool bIsChinese;
};

#endif // MAINWINDOW_H
