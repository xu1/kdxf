#ifndef CTMPLAYTTS_H
#define CTMPLAYTTS_H

#include <QObject>

class CTMPlayTTS : public QObject
{
    Q_OBJECT
public:
    explicit CTMPlayTTS(QObject *parent = 0);

signals:
    void sig_updateUI(QString strTxt);

public slots:
    void slot_playTTS(QString strTxt);
};

#endif // CTMPLAYTTS_H
