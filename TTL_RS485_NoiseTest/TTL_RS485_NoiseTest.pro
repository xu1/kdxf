#-------------------------------------------------
#
# Project created by QtCreator 2016-11-01T16:50:52
#
#-------------------------------------------------

QT       += core gui
QT       += serialport xml multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TTL_RS485_NoiseTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    modbus_rtn/tmserialwork.cpp \
    modbus_rtn/ctmasrtool.cpp \
    modbus_rtn/ctmaudioctrl.cpp \
    ctmglobalctrl.cpp \
    modbus_rtn/ctmmediactrl.cpp \
    ctmplaytts.cpp

HEADERS  += mainwindow.h \
    modbus_rtn/tmserialwork.h \
    modbus_rtn/ctmasrtool.h \
    modbus_rtn/ctmaudioctrl.h \
    ctmglobalctrl.h \
    modbus_rtn/ctmmediactrl.h \
    ctmplaytts.h

FORMS    += mainwindow.ui

# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lmsc

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf

# lib kdxf
win32: LIBS += -L$$PWD/libkdxf/ -lsharedlib_kdxf

INCLUDEPATH += $$PWD/libkdxf
DEPENDPATH += $$PWD/libkdxf

# lib zeromq
win32: LIBS += -L$$PWD/libzmq -llibzmq_x

INCLUDEPATH += $$PWD/libzmq
DEPENDPATH += $$PWD/libzmq


