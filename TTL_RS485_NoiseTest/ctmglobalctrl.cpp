#include "ctmglobalctrl.h"

QString g_strRootDir = "";
QSettings* g_pSettingsIni = NULL;
void* g_pZmqCtx = NULL;


CTMAudioCtrl* g_pCtrlAudio = NULL;
CTMMediaCtrl* g_pCtrlMedia = NULL;
CTmAsrTool* g_pAsrTool = NULL;
CTMPlayTTS* g_pPlayTTS = NULL;


void g_clearDir(QString strDir)
{
    QDir dirRoot(strDir);
    if( dirRoot.exists() ){
        dirRoot.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        QFileInfoList fileList = dirRoot.entryInfoList();
        foreach (QFileInfo fi, fileList)
        {
            dirRoot.remove(fi.fileName());
        }
    }
}

QString g_strGetMD5(QString strTxt)
{
    QString strMD5;
    QCryptographicHash md5(QCryptographicHash::Md5);
    md5.addData(strTxt.toUtf8());
    QByteArray bArry = md5.result();
    QString strChar;
    for( int i=0;i<bArry.size();i++ ){
        char cNum = bArry.at(i);
        strChar.sprintf("%02X", (uchar)cNum);
        strMD5 += strChar;
    }
    return strMD5;
}

int g_nGetRandom(int nMin, int nMax)
{
    if( (nMax-nMin) == 0 ){
        return nMin;
    }else if(nMin > nMax){
        return nMax;
    }
    qsrand(QTime(0,0,0).msecsTo(QTime::currentTime()));
    //int n = (qrand()%((nMax-nMin+1)*100)) / 100 + nMin;
    int n = qrand()%(nMax-nMin+1) + nMin;
    return n;
}
