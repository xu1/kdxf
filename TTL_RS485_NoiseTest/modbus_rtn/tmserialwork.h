﻿#ifndef TAISERICALWORK_H
#define TAISERICALWORK_H

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QSerialPort>
#include <QQueue>
#include <QMutex>
#include <QThread>
#include <QDebug>

class TmSerialWorkThread;

// to work in the thread
class TmSerialWork : public QObject
{
    Q_OBJECT
public:
    explicit TmSerialWork(QObject *parent=0);

    void setParent(void* parent);
    void threadLoop();
    void exit();

    bool m_bInitOk;
    bool m_bEnableCtrl;
    int m_nPersonDistance[2];

private:
    int bytesToInt(QByteArray& bytes);
private:
    TmSerialWorkThread* pr_pThreadWork;
    QSerialPort* m_pSer;

    QQueue<QByteArray> m_ctrlList;
    QMutex m_lock;
    QByteArray m_serBuff;

    QString m_szLog;
    void     *m_parent;
signals:
    void signalvoicedb(double);
private slots:
    void slotser_newdata();
public slots:
    void slotInitSerial(QString strCom);
    void slotWriteSerialData(QByteArray bArry);
};

// the work thread
class TmSerialWorkThread : public QThread
{
    Q_OBJECT
public:
    explicit TmSerialWorkThread(TmSerialWork* pWork);
    bool m_bExit;
private:
    TmSerialWork* pr_pWork;
    virtual void run();

signals:

public slots:
};

#endif // TAISERICALWORK_H
