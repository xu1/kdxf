#include "ctmasrtool.h"
#include "../ctmglobalctrl.h"

CTmAsrTool::CTmAsrTool(QObject *parent) : QObject(parent)
{
    DelayTime = g_pSettingsIni->value("ASR/DelayTime", 0).toInt();
    bDebug = g_pSettingsIni->value("ASR/PCM", 0).toInt();
    QString strGramaName = g_pSettingsIni->value("ASR/GramaName","tami").toString();
    pr_pAsrSDK = new Sharedlib_kdxf();
    if(!pr_pAsrSDK->login("556d116e")){
        qDebug()<<"KDXF SDK login Failed!";
    }
    connect(this, &CTmAsrTool::sig_get_result, this, &CTmAsrTool::slot_get_result);
    setGramaName(strGramaName);
    buildConfig(strGramaName);
    qDebug()<<DelayTime<<strGramaName;
}

void CTmAsrTool::startAsr(int nTime)
{
    pr_pAsrSDK->isr_start(true, false);
    pr_nTimeStart = QTime::currentTime().addMSecs(DelayTime);
    pr_nTimeEnd = pr_nTimeStart.addMSecs(nTime);
//    qDebug()<<"Start ASR:"<<pr_nTimeStart<<pr_nTimeEnd;
    pr_pAudioPCM = NULL;
    if( bDebug ){
        QString strTmp = QTime::currentTime().toString("hhmmsszzz")+".pcm";
        pr_pAudioPCM = fopen(strTmp.toLocal8Bit().data(), "wb");
    }
}

void CTmAsrTool::stopAsr()
{
    QTime curTime = QTime::currentTime();
    if( curTime < pr_nTimeEnd ){       
        pr_nTimeEnd = curTime;

    }
}

bool CTmAsrTool::buildConfig(QString strFileName)
{
    // build
    return pr_pAsrSDK->isr_build_bnf(strFileName);
}

void CTmAsrTool::setGramaName(QString strName)
{
    pr_pAsrSDK->isr_setGramID(strName);
}
//FILE* pTest = fopen("asr.pcm","wb");
void CTmAsrTool::slot_audio_data_in(QByteArray bArray)
{
    static bool bIsStop = true;
    QTime curTime = QTime::currentTime();
    if( curTime >= pr_nTimeEnd ){
        if( !bIsStop ){
            qDebug()<<"xinhao====";
            emit sig_get_result();
            if( pr_pAudioPCM ){
                fclose(pr_pAudioPCM);
                pr_pAudioPCM = NULL;
            }
        }
        bIsStop = true;
        return;
    }
    if( curTime >= pr_nTimeStart){
        bIsStop = false;
        pr_pAsrSDK->isr_updateAudio(bArray.data(), bArray.length());
        if(pr_pAudioPCM){
            fwrite(bArray.data(), 1, bArray.length(), pr_pAudioPCM);
        }
    }

}

void CTmAsrTool::slot_get_result()
{
    QString strResultXML = pr_pAsrSDK->isr_getResult();
//    qDebug()<<"ASR Result:"<<strResultXML.toUtf8().data();
    pr_pAsrSDK->isr_stop();
    int nConfidence = 0;
    QString strResultWord = "";
//    emit sig_asr_over(strResult);
    if( strResultXML.length()>0 ){
        QDomDocument xmlDoc;
        QString strError;
        int nErrorLine, nErrorColum;
        if( !xmlDoc.setContent(strResultXML, &strError, &nErrorLine, &nErrorColum) ){
            qDebug()<< QString("Xml Parse Error! [%1:%2] %3").arg(nErrorLine).arg(nErrorColum).arg(strError);
            return;
        }
        // pase obj
        QDomElement root = xmlDoc.documentElement();
        if( root.isNull() ){
            qDebug()<<"xml Root is NULL!";
            return;
        }

        QDomNodeList nodelist = root.elementsByTagName("confidence");
        if( nodelist.length()<1 ){
            qDebug()<<"Nodelist is null";
            return;
        }
        QString strConfidence = nodelist.at(0).toElement().text();
        nConfidence = strConfidence.toInt(); // 返回可信度
        // result
        nodelist = root.elementsByTagName("result");
        if( nodelist.length()>0 ){
            nodelist = nodelist.at(0).toElement().elementsByTagName("object");
            if( nodelist.length()>0 ){
                QDomNodeList objList = nodelist.at(0).toElement().childNodes();
                QDomElement oneEle;
                for(int i=0; i<objList.size();i++){
                    oneEle = objList.at(i).toElement();
                    if( oneEle.tagName() == "ask" ){
                        strResultWord = oneEle.text();
                        break;
                    }
                }
            }
        }

    }
    qDebug()<<"================================"<<nConfidence<<strResultWord;
    emit sig_asr_over(nConfidence, strResultWord);

}

