#ifndef CTMAUDIOCTRL_H
#define CTMAUDIOCTRL_H

#include <QObject>
#include <QThread>
#include <QAudioInput>
#include <QAudioOutput>
#include <QAudioFormat>
#include <QFile>

#define MAX_BUFF_SIZE 1280

class CTMAudioInThread;
class CTMAudioOutThread;
class CTMAudioCtrl : public QObject
{
    Q_OBJECT
public:
    explicit CTMAudioCtrl(QObject *parent = 0);
    void init();

private:
    CTMAudioInThread* pr_pAudioInThread;
    CTMAudioOutThread* pr_pAudioOutThread;
signals:

public slots:
};

class CTMAudioOutputDevice : public QIODevice
{
public:
    CTMAudioOutputDevice(){
        open(QIODevice::ReadWrite);
    }
    virtual qint64 readData(char *data, qint64 maxlen){
        if( pr_buffList.size()<1 ){
            memset(data, 256*128, MAX_BUFF_SIZE); // 8bit 128 is slience
            return MAX_BUFF_SIZE;
        }
        //qDebug()<<"Read Buff:"<<pr_buffList.size();
        // take one
        QByteArray bArray = pr_buffList.takeAt(0);
        int nBuffSize = bArray.size();
        if( nBuffSize > maxlen ){
            memcpy(data, bArray.data(), maxlen);
            pr_buffList.push_front(bArray.right(nBuffSize-maxlen));
            return maxlen;
        }else{
            memcpy(data, bArray.data(), nBuffSize);
            return nBuffSize;
        }
    }
    virtual qint64 writeData(const char *data, qint64 len){
        if( pr_buffList.size() > 30 ){
            pr_buffList.clear();
        }
//        qDebug()<<"Write Buff:"<<pr_buffList.size();
        QByteArray bArray;
        for(int i=0; i<len/MAX_BUFF_SIZE; i++){
            bArray = QByteArray(data+i*MAX_BUFF_SIZE, MAX_BUFF_SIZE);
            pr_buffList.push_back(bArray);
        }
        int nLastLen = len%MAX_BUFF_SIZE;
        if( nLastLen > 0 ){
            bArray = QByteArray(data+len-nLastLen, nLastLen);
            pr_buffList.push_back(bArray);
        }
        return len;
    }
private:
    QList<QByteArray> pr_buffList;
};
class CTMAudioInThread : public QThread
{
    Q_OBJECT
public:
    explicit CTMAudioInThread();
    void run();
    void setExit();
private:
    bool pr_bExit;
    void* pr_nSocketPull;
    QString pr_strPullAddr;
    QAudioOutput* pr_pAudioOutput;
    CTMAudioOutputDevice* pr_pAudioDeviceOut;

signals:


public slots:
};

class CTMAudioOutThread : public QThread
{
    Q_OBJECT
public:
    explicit CTMAudioOutThread();
    void run();
    void setExit();
private:
    bool pr_bExit;
    void* pr_nSocketPub;
    QString pr_strPubAddr;
    QAudioInput* pr_pAudioInput;
    QIODevice* pr_pAudioDeviceIn;
signals:
    void sig_ASR_AudioData(QByteArray audioData);

private slots:
    void slot_audio_data();
};

#endif // CTMAUDIOCTRL_H
