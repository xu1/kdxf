#ifndef CTMASRTOOL
#define CTMASRTOOL

#include <QObject>
#include <QtCore>
#include <QDomDocument>
#include "libkdxf/sharedlib_kdxf.h"

class CTmAsrTool : public QObject
{
    Q_OBJECT
public:
    explicit CTmAsrTool(QObject *parent = 0);
    //开始语音识别
    void startAsr(int nTime);
    //停止语音识别
    void stopAsr();
    //编译语法文件
    bool buildConfig(QString strFileName);
    //设置语法名字
    void setGramaName(QString strName);

    Sharedlib_kdxf* pr_pAsrSDK;
private:    
    QTime pr_nTimeEnd, pr_nTimeStart;
    FILE* pr_pAudioPCM;
    int DelayTime;
    bool bDebug;
signals:
    void sig_asr_over(int nConfidence, QString strName);
    //获取识别结果信号
    void sig_get_result();
public slots:
    //获取音频数据并创建音频文件
    void slot_audio_data_in(QByteArray bArray);
    //获取识别结果
    void slot_get_result();
};

#endif // CTMASRTOOL

