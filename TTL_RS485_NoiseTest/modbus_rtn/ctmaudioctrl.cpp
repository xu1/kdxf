#include "ctmaudioctrl.h"
#include <QCoreApplication>
#include "../ctmglobalctrl.h"

CTMAudioCtrl::CTMAudioCtrl(QObject *parent) : QObject(parent)
{
    pr_pAudioInThread = NULL;
    pr_pAudioOutThread = NULL;

}

void CTMAudioCtrl::init()
{
    if( !pr_pAudioInThread ){
        pr_pAudioInThread = new CTMAudioInThread();
        pr_pAudioInThread->start();

    }
    QThread::msleep(80);
    if( !pr_pAudioOutThread ){
        pr_pAudioOutThread = new CTMAudioOutThread();
        pr_pAudioOutThread->start();
        connect(pr_pAudioOutThread,SIGNAL(sig_ASR_AudioData(QByteArray)),
                g_pAsrTool,SLOT(slot_audio_data_in(QByteArray)),Qt::QueuedConnection);
    }
}

// Audio In
CTMAudioInThread::CTMAudioInThread()
{
    pr_bExit = false;
    pr_pAudioDeviceOut = NULL;
    pr_pAudioOutput = NULL;
    pr_nSocketPull = NULL;
}

void CTMAudioInThread::run()
{
    // init pull
    pr_strPullAddr = g_pSettingsIni->value("Channels/AudioIn").toString();
    pr_nSocketPull = zmq_socket(g_pZmqCtx, ZMQ_PULL);
    int nVal = 30;
    zmq_setsockopt(pr_nSocketPull, ZMQ_RCVTIMEO, &nVal, sizeof(nVal));
    nVal = 0;
//    zmq_setsockopt(pr_nSocketPull, ZMQ_LINGER, &nVal, sizeof(nVal));
    nVal = 50;
    zmq_setsockopt(pr_nSocketPull, ZMQ_RCVHWM, &nVal, sizeof(nVal));
    zmq_bind(pr_nSocketPull, pr_strPullAddr.toLocal8Bit().data());
//    printf("AudioIn Addr: [PULL] %s, id: %d\n", pr_strPullAddr.toLocal8Bit().data(), pr_nSocketPull);
//    emit g_pDlgMain->sig_showLog(QString("AudioIn Addr: [PULL] %1, id: %2").arg(pr_strPullAddr).arg((int)pr_nSocketPull));
    // init audio in
    if( !pr_pAudioOutput ){
        QAudioFormat format;
        // Set up the desired format, for example:
        format.setSampleRate(16000);
        format.setChannelCount(1);
        format.setSampleSize(16);
        format.setCodec("audio/pcm");
        format.setByteOrder(QAudioFormat::LittleEndian);
        format.setSampleType(QAudioFormat::SignedInt);
        QAudioDeviceInfo info = QAudioDeviceInfo::defaultOutputDevice();
        if (!info.isFormatSupported(format)) {
           //qWarning() << "Default format not supported, trying to use the nearest.";
           format = info.nearestFormat(format);
        }
        pr_pAudioOutput = new QAudioOutput(format);
        pr_pAudioDeviceOut = new CTMAudioOutputDevice();
        pr_pAudioOutput->start(pr_pAudioDeviceOut);
//        printf("AudioIn Open. %s. 16000 Hz, 16 Bit, 1 Channel.\n", info.deviceName().toLocal8Bit().data());
//        emit g_pDlgMain->sig_showLog(QString("AudioIn Open. %1. 16000 Hz, 16 Bit, 1 Channel.").arg(info.deviceName()));
    }
    // loop
    zmq_msg_t msg;
    while(!pr_bExit){
        msleep(10);
        QCoreApplication::processEvents();
        zmq_msg_init(&msg);
        int nLen = zmq_msg_recv(&msg, pr_nSocketPull, ZMQ_DONTWAIT); // ZMQ_DONTWAIT
        if( nLen > 0 ){
            char* buff = (char*)zmq_msg_data(&msg);
            pr_pAudioDeviceOut->writeData(buff, nLen);

        }
        zmq_msg_close(&msg);
    }

    // close
    if( pr_pAudioOutput ){
        pr_pAudioOutput->stop();
        pr_pAudioOutput = NULL;
    }
    if( pr_pAudioDeviceOut ){
        pr_pAudioDeviceOut->close();
        pr_pAudioDeviceOut = NULL;
    }
    zmq_close(pr_nSocketPull);
    pr_nSocketPull = NULL;

}

void CTMAudioInThread::setExit()
{
    pr_bExit = true;
}

// Audio Out
CTMAudioOutThread::CTMAudioOutThread()
{
    pr_bExit = false;
    pr_pAudioDeviceIn = NULL;
    pr_pAudioInput = NULL;
    pr_nSocketPub = NULL;
}

void CTMAudioOutThread::run()
{
    // init pull
    pr_strPubAddr = g_pSettingsIni->value("Channels/AudioOut").toString();
    pr_nSocketPub = zmq_socket(g_pZmqCtx, ZMQ_PUB);
    int nVal = 50;
    zmq_setsockopt(pr_nSocketPub, ZMQ_SNDTIMEO, &nVal, sizeof(nVal));
    nVal = 0;
//    zmq_setsockopt(pr_nSocketPub, ZMQ_LINGER, &nVal, sizeof(nVal));
    nVal = 30;
    zmq_setsockopt(pr_nSocketPub, ZMQ_SNDHWM, &nVal, sizeof(nVal));
    zmq_bind(pr_nSocketPub, pr_strPubAddr.toLocal8Bit().data());
//    printf("AudioOut Addr: [PUB] %s, id: %d\n", pr_strPubAddr.toLocal8Bit().data(), pr_nSocketPub);
//    emit g_pDlgMain->sig_showLog(QString("AudioOut Addr: [PUB] %1, id: %2").arg(pr_strPubAddr).arg((int)pr_nSocketPub));

    // init audio out
    if( !pr_pAudioInput ){
        QAudioFormat format;
        // Set up the desired format, for example:
        format.setSampleRate(16000);
        format.setChannelCount(1);
        format.setSampleSize(16);
        format.setCodec("audio/pcm");
        format.setByteOrder(QAudioFormat::LittleEndian);
        format.setSampleType(QAudioFormat::SignedInt);
        QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
        if (!info.isFormatSupported(format)) {
           //qWarning() << "Default format not supported, trying to use the nearest.";
           format = info.nearestFormat(format);
        }
        pr_pAudioInput = new QAudioInput(format);
        pr_pAudioDeviceIn = pr_pAudioInput->start();
        connect(pr_pAudioDeviceIn, SIGNAL(readyRead()), this, SLOT(slot_audio_data()), Qt::QueuedConnection);
//        printf("AudioOut Open. %s. 16000 Hz, 16 Bit, 1 Channel.\n", info.deviceName().toLocal8Bit().data());
//        emit g_pDlgMain->sig_showLog(QString("AudioOut Open. %1. 16000 Hz, 16 Bit, 1 Channel.").arg(info.deviceName()));
    }
    // loop
    while(!pr_bExit){
        msleep(10);
        QCoreApplication::processEvents();
    }

    // close
    if( pr_pAudioInput ){
        pr_pAudioInput->stop();
        pr_pAudioInput = NULL;
    }
    if( pr_pAudioDeviceIn ){
        pr_pAudioDeviceIn->close();
        pr_pAudioDeviceIn = NULL;
    }
    zmq_close(pr_nSocketPub);
    pr_nSocketPub = NULL;
}

void CTMAudioOutThread::setExit()
{
    pr_bExit = true;
}

void CTMAudioOutThread::slot_audio_data()
{
    if( pr_pAudioInput && pr_pAudioDeviceIn ){
        QByteArray bArray;
        bArray = pr_pAudioDeviceIn->readAll();
        emit sig_ASR_AudioData(bArray);
    }
}
