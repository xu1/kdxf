﻿#include "tmserialwork.h"
#include <QCoreApplication>

///////////////////////////////////////////////////////////////////////////////
/// \brief TmSerialWork::TmSerialWork
/// \param parent
//////////////////////////////////////////////////////////////////////////////
TmSerialWork::TmSerialWork(QObject *parent) : QObject(parent)
{
    m_bEnableCtrl = true;
    m_bInitOk = false;
    m_nPersonDistance[0] = 0;
    m_nPersonDistance[1] = 0;

    pr_pThreadWork = new TmSerialWorkThread(this);
    this->moveToThread(pr_pThreadWork);
    pr_pThreadWork->start();
}

void TmSerialWork::setParent(void* parent)
{
    m_parent = parent;
}

void TmSerialWork::threadLoop()
{
    if (!m_bInitOk)
        return;
    // queue read
    m_lock.lock();
    if (!m_ctrlList.isEmpty())
    {
        QByteArray bArry = m_ctrlList.dequeue();
        if (m_bEnableCtrl)
        {
           m_pSer->write(bArry);
        }
    }
    m_lock.unlock();
}

void TmSerialWork::exit()
{
    pr_pThreadWork->m_bExit = true;
    pr_pThreadWork->quit();
    if(pr_pThreadWork->wait(1000))
    {
        pr_pThreadWork->terminate();
        pr_pThreadWork->wait(1000);
    }
    m_pSer->close();
    m_pSer->deleteLater();
}

void TmSerialWork::slotser_newdata()
{
    //01 03 02 01 E5 78 5F
    //01 03 02 01 FA 39 97
    //01 03 02 01 EF F8 58
    m_serBuff += m_pSer->readAll();
//    qDebug() << "newdata=" << m_serBuff;
    if (m_serBuff.length() >= 7)
    {
        //截取前面8个字节
//        int nhead = find_cmd(m_serBuff.data(), m_serBuff.length(), 0x01, 0x0A);
        m_serBuff.simplified();
        QByteArray array = m_serBuff.left(7);
        while (array.length() > 0)
        {
            //...
            //bArry[0]地址位  bArry[1]命令字
            if( array[0] == char(0x01) && array[1] == char(0x03) )
            {
                //数据长度array[2]
                if( array[2] == char(0x02) )
                {
                    QByteArray byte;
                    byte.resize(2);
                    byte[0] = array[4];    // 数据
                    byte[1] = array[3];    // 数据  //因为存在大小尾端问题，ba实际存储的是 byte[1]byte[0]

                    int ndb = bytesToInt(byte);
//                    qDebug()<<ndb;
                    emit signalvoicedb(ndb/10.0);
                }
            }
            m_serBuff = m_serBuff.right(m_serBuff.length() - 7);
            array = m_serBuff.left(7);
        }
    }
}

void TmSerialWork::slotInitSerial(QString strCom)
{
    // init serial port
    m_pSer = new QSerialPort();
    m_pSer->setPortName(strCom);
    m_pSer->setBaudRate(QSerialPort::Baud9600);
    m_pSer->setDataBits(QSerialPort::Data8);
    m_pSer->setStopBits(QSerialPort::OneStop);
    m_pSer->setParity(QSerialPort::NoParity);

    m_bInitOk = m_pSer->open(QIODevice::ReadWrite);

    if (!m_bInitOk)
    {
        m_szLog = QString("串口%0打开失败，请检查接口!").arg(strCom);
        qDebug() <<m_szLog.toUtf8().data();
        return;
    }
    else
    {
        m_szLog = QString("串口%0,打开成功!").arg(strCom);
        qDebug() << m_szLog.toUtf8().data();
    }
    // recv data
    connect(m_pSer, &QSerialPort::readyRead, this, &TmSerialWork::slotser_newdata, Qt::QueuedConnection);
}

void TmSerialWork::slotWriteSerialData(QByteArray bArry)
{
    if (!m_bEnableCtrl || !m_bInitOk)
        return;
    m_lock.lock();

    if (m_ctrlList.size() > 32)        
    {
        m_ctrlList.clear();
    }

    m_ctrlList.enqueue(bArry);
    m_lock.unlock();    
}
/***************************************************************************************
 *
 *
 *
 ***************************************************************************************/

TmSerialWorkThread::TmSerialWorkThread(TmSerialWork *pWork)
{
    pr_pWork = pWork;
    m_bExit = false;
}

void TmSerialWorkThread::run()
{
    while (!m_bExit)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 20);
        msleep(30);
        pr_pWork->threadLoop();
    }
}

int TmSerialWork::bytesToInt(QByteArray &bytes)
{
    int addr = bytes[0] & 0x000000FF;
    addr |= ((bytes[1] << 8)  &  0x0000FF00);
    addr |= ((bytes[2] << 16) &  0x00FF0000);
    addr |= ((bytes[3] << 24) &  0xFF000000);

    return addr;
 }
