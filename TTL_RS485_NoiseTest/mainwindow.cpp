﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    g_pZmqCtx = zmq_ctx_new();
    g_strRootDir = QCoreApplication::applicationDirPath();
    QDir::setCurrent(g_strRootDir);
    g_pSettingsIni = new QSettings("./Config/Robot.ini", QSettings::IniFormat);
    g_pSettingsIni->setIniCodec("UTF-8");
    m_timeCounts = g_pSettingsIni->value("SpaceTime/time", 0).toInt();
    minValue = g_pSettingsIni->value("NoiseValue/minValue", 55).toInt();
    maxValue = g_pSettingsIni->value("NoiseValue/maxValue", 65).toInt();
    qDebug()<<"minValue:"<<minValue<<"maxValue:"<<maxValue<<"m_timeCounts:"<<m_timeCounts<<g_strRootDir;
    nMax = 0;
    nMin = 0;
    nMid = 0;
    nAsrCount = 0;
    nCounts = 0;
    m_bAsrStart = true;
    m_bStart = false;
    m_bStop = true;
    ui->pushButton_asr->hide();
    ui->label_9->setText("检测中。。。");
    Init();
}

MainWindow::~MainWindow()
{
    UnInit();
    delete ui;
}



void MainWindow::Init()
{
    pr_pSerWork = new TmSerialWork();
    connect(this,&MainWindow::sigInitSerial,pr_pSerWork,&TmSerialWork::slotInitSerial,Qt::QueuedConnection);
    connect(this, &MainWindow::sigWriteSerialData, pr_pSerWork, &TmSerialWork::slotWriteSerialData,Qt::QueuedConnection);

    connect(pr_pSerWork, &TmSerialWork::signalvoicedb, this, &MainWindow::slotVoicedb,Qt::QueuedConnection);

    connect(&m_timer,&QTimer::timeout,this,&MainWindow::slotTimerout,Qt::QueuedConnection);
    emit sigInitSerial("COM1");
    m_timer.start(1000);

    g_pAsrTool = new CTmAsrTool();
    connect(g_pAsrTool,SIGNAL(sig_asr_over(int,QString)),this,SLOT(slot_asr_result(int,QString)));
    QThread::msleep(80);
    g_pCtrlAudio = new CTMAudioCtrl();
    g_pCtrlAudio->init();

    QThread::msleep(100);
    if(!g_pCtrlMedia){
        g_pCtrlMedia = new CTMMediaCtrl();
        g_pCtrlMedia->init();
    }

    QThread::msleep(100);
    g_pPlayTTS = new CTMPlayTTS();
    QThread *myThread = new QThread();
    g_pPlayTTS->moveToThread(myThread);
    myThread->start();
    connect(g_pPlayTTS,SIGNAL(sig_updateUI(QString)),this,SLOT(slot_updateUI(QString)));
    connect(this,SIGNAL(sig_playTTS(QString)),g_pPlayTTS,SLOT(slot_playTTS(QString)));

    pr_timer = new QTimer();
    connect(pr_timer,SIGNAL(timeout()),this,SLOT(slot_timeout()));
//    pr_timer->start(1000);
    ui->label_4->setText(QString("0/0/0/0 %1s").arg(m_timeCounts));
    ui->label_6->setText("关");

}

void MainWindow::UnInit()
{
    m_timer.stop();
    pr_timer->stop();
    pr_pSerWork->exit();
    g_pCtrlMedia->unInit();
}

void MainWindow::slotTimerout()
{
    if(!pr_pSerWork->m_bInitOk)
        return;
    //01 03 00 00 00 01 84 0A
    unsigned char buff[] = { 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x84, 0x0A };

    emit sigWriteSerialData(QByteArray((const char *)buff, 8));
    //qDebug() << "send buff=" << buff;
    nCounts++;
}

void MainWindow::slotVoicedb(double db)
{
    QString str = "当前分贝值为:"+QString::number(db);
    ui->textEdit_2->append(str);
    qDebug()<<"nCounts:"<<nCounts<<nMax<<nMid<<nMin;
//    qDebug()<<str.toStdString().data();
    if(nCounts <= m_timeCounts){
        if(db > maxValue){
            nMax++;
        }
        if(db < minValue){
            nMin++;
        }
        if(db >= minValue && db <= maxValue){
            nMid++;
        }

    }
    else{        
        float maxResult = float(nMax)/float(nMax+nMin+nMid);
        float midResult = float(nMid)/float(nMax+nMin+nMid);
        float minResult = float(nMin)/float(nMax+nMin+nMid);
        ui->label_4->setText(QString("%1/%2/%3/%4 %5s").arg(nMax).arg(nMid).arg(nMin).arg(nMax+nMin+nMid).arg(m_timeCounts));
        qDebug()<<"===result==="<<maxResult << midResult << minResult;
        if( maxResult >= midResult && maxResult > minResult){
            m_bAsrStart = false;
            ui->pushButton_asr->hide();
            ui->label_6->setText("关");
            ui->label_9->setText("高噪(大于65分贝)");
            ui->label_11->setText("关闭");
            ui->label_7->clear();
        }
        if(midResult > maxResult && midResult >= minResult){
            m_bAsrStart = false;
            ui->pushButton_asr->show();
            ui->label_6->setText("开");
            ui->label_9->setText("中噪(55-65分贝)");
            ui->label_11->setText("按钮触发识别");
            ui->label_7->setText("结束");
        }
        if(minResult > maxResult && minResult > midResult){
            m_bAsrStart = true;
            ui->pushButton_asr->hide();
            pr_timer->start(1000);
            nAsrCount = 0;
            ui->label_6->setText("开");
            ui->label_9->setText("低噪(小于55分贝)");
            ui->label_11->setText("自动识别");
        }
        nCounts = 0;
        nMax = 0;
        nMin = 0;
        nMid = 0;
//        ui->label_4->clear();
    }
}

void MainWindow::slot_asr_result(int nConfidence, QString strResult)
{
//    ui->textEdit->append(strResult);

    m_nConfidence = nConfidence;
    m_bResult = strResult;

    if(m_nConfidence > 0){
        ui->textEdit->append(QString("%1 %2").arg(m_bResult).arg(m_nConfidence));
        ui->label_7->setText("播放tts");
        emit sig_playTTS(m_bResult);
    }
    else{
        ui->label_7->setText("未识别");
    }


}

void MainWindow::on_pushButton_clicked()
{
    ui->textEdit_2->clear();
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->textEdit->clear();
}

void MainWindow::slot_timeout()
{   
    qDebug()<<"--nAsrCount--"<<nAsrCount;
    nAsrCount++;
    if(m_bAsrStart){
        if(!m_bStart){
            g_pAsrTool->startAsr(30000);
            ui->label_7->setText("开始");
            m_bStart = true;
        }
        if(nAsrCount >= 3){
            g_pAsrTool->stopAsr();
            m_bStart = false;
            nAsrCount = 0;
        }
    }
}


void MainWindow::on_pushButton_asr_pressed()
{
    g_pAsrTool->startAsr(30000);
    ui->pushButton_asr->setText("松开停止");
    ui->label_7->setText("开始");
}

void MainWindow::on_pushButton_asr_released()
{
    ui->pushButton_asr->setText("按下说话");
    g_pAsrTool->stopAsr();

}

void MainWindow::slot_updateUI(QString strTxt)
{
    ui->label_7->setText(strTxt);

}
