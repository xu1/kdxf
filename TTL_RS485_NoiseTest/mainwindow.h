﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "./modbus_rtn/tmserialwork.h"
#include "ctmglobalctrl.h"
#include <QThread>
#include <QTimer>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void sigInitSerial(QString);
    void sigWriteSerialData(QByteArray);

    void sig_playTTS(QString strTxt);

private slots:
    void slotTimerout();

    void slotVoicedb(double db);

    void slot_asr_result(int nConfidence, QString strResult);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void slot_timeout();

    void on_pushButton_asr_pressed();

    void on_pushButton_asr_released();

    void slot_updateUI(QString strTxt);

private:
    Ui::MainWindow *ui;
    void Init();
    void UnInit();
    TmSerialWork* pr_pSerWork;
    QTimer m_timer;
    bool m_bStart;
    bool m_bStop;
    int m_timeCounts;
    int nCounts;
    QTimer *pr_timer;
    int nMax;
    int nMin;
    int nMid;
    int nAsrCount;
    bool m_bAsrStart;
    int m_nConfidence;
    QString m_bResult;
    int minValue;
    int maxValue;
};

#endif // MAINWINDOW_H
